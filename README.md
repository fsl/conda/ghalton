# Conda recipe for ghalton

This recipe is hosted at https://git.fmrib.ox.ac.uk/fsl/conda/ghalton.

It builds the `ghalton` library, hosted at https://github.com/fmder/ghalton.

```
conda build -c conda-forge --output-folder=<path-to-channel-directory> .
```
